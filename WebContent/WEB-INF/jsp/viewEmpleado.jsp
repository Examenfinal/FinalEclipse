<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
	<head>
	<meta charset="utf-8" />
	<title>Color Admin | Cliente</title>
	  	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
		<!-- 
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" />
		-->
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/animate/animate.min.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/css/default/style.min.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/css/default/style-responsive.min.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/css/default/theme/default.css" rel="stylesheet" id="theme" />		
		<!-- ================== END BASE CSS STYLE ================== -->
		
		<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
		
		<!-- ================== BEGIN BASE JS ================== -->
		<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/pace/pace.min.js"></script>
		<!-- ================== END BASE JS ================== -->

	  	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>  
	</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	

<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="index.html" class="navbar-brand"><span class="navbar-logo"></span> <b>Color</b> Admin</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li>
					<form class="navbar-form">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Enter keyword" />
							<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
						</div>
					</form>
				</li>			
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<img src="https://seantheme.com/color-admin-v4.0/admin/assets/img/user/user-13.jpg" alt="" /> 
						<span class="d-none d-md-inline">Cliente</span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="javascript:;" class="dropdown-item">Edit Profile</a>
						<div class="dropdown-divider"></div>
						<a href="Logout" class="dropdown-item">Log Out</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<jsp:include page="includeSideBar.jsp"/>
		<!-- end #sidebar -->
		
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item"><a href="javascript:;">Mantenimiento</a></li>
				<li class="breadcrumb-item active">Cliente</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Cliente <small>Mantenimiento de Cliente...</small></h1>
			<!-- end page-header -->
			
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">						
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" id="btnPageNewRow"><i class="fa fa-plus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" id="btnPageRefreshRows"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="Xls" class="btn btn-xs btn-icon btn-circle btn-primary"><i class="fa fa-file-excel"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Data Table - Default</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
				
				
				<!-- INICIO S502CI -->

<table id="data-table-default" class="table table-striped table-bordered">
						<thead>							
							<th></th>
							<th></th>
							<th>Codigo</th>							
							<th>nombre</th>							
							<th>apellido1</th>
							<th>apellido2</th>
							<th>telefono</th>
							<th>direccion</th>
							<th>estado</th>
							<th></th>
						</thead>
						<tbody>
										                 
							
					
						</tbody>
					</table>

	<!-- FIN S502CI -->
					
					
							
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end #content -->

		<!-- #modal-dialog -->
		<div class="modal fade" id="modal-dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Empleado</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						    <div class="alert alert-secondary">
                        		Porfavor ingrese todos los <code></code>datos, necesarios.
                        	</div>
                        	<form id="formModal">
                        		<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Codigo</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalCodigo" placeholder="Codigo" />
										<input type="hidden" id="hddModalCodigo"/>
										<small class="f-s-12 text-grey-darker">Automatico.</small>
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Nombre</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalNombre" placeholder="Nombre Completo" />
										
									</div>
								</div>
							
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Apellido Materno</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalApellido" placeholder="Apellido Completo" />
									</div>
								</div>	
								
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Apellido Paterno</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalApellido" placeholder="Apellido Completo" />
									</div>
								</div>	
															
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Telefono</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalTelefono" placeholder="Telefono" />
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Direccion</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalDireccion" placeholder="Direccion" />
										
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Estado:</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalEstado" placeholder="Estado" />
										
									</div>
								</div>
							</form>
					</div>
					<div class="modal-footer">
						<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
						<a href="javascript:;" class="btn btn-success" id="btnModalSave">Guardar</a>
					</div>
				</div>
			</div>
		</div>
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<!--[if lt IE 9]>
		<script src="https://seantheme.com/color-admin-v4.0/admin/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="https://seantheme.com/color-admin-v4.0/admin/assets/crossbrowserjs/respond.min.js"></script>
		<script src="https://seantheme.com/color-admin-v4.0/admin/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/gritter/js/jquery.gritter.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/js-cookie/js.cookie.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/js/theme/default.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/js/apps.min.js"></script>
	
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/js/demo/table-manage-default.demo.min.js"></script>	
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	
	<script>
		//var listadoCliente = ${listadoCliente};
	
		$(document).ready(function() {
			App.init();
			//TableManageDefault.init();
			
			// CARGAR COMBOBOX
			//var $element = $('#inpModalEstado');			
			//$.each(listadoEstados, function(i, dato) {			
			//	var $option = $("<option/>").attr("value", dato.par_argume).text(dato.par_funcion);
			//	$element.append($option);				
			//});
			//-------------------------------------------------------
			
			// CONFIGURAR DATATABLE
			var dt = $("#data-table-default").DataTable({
				responsive:!0,
				columns:[
						{data : null,'defaultContent': '', 'orderable': false, 
							'render': function ( data, type, full, meta ) {
								return  meta.row + 1;
							} 
						},
						{data : null,'defaultContent':'<input type="checkbox">'},						
						{data : "emp_codigo"},
						{data : "emp_nombre"},
						{data : "emp_apellido1"},
						{data : "emp_apellido2"},
						{data : "emp_telefono"},
						{data : "emp_direccion"},
						{data : "emp_estado"},
						{data : "emp_codigo",'orderable': false, 
							'render': function ( data, type, full, meta ) {
								var html ='<button class="btn btn-xs btn-danger" id="btnPageDeleteRow" data-codigo="' + data + '"><i class="fa fa-trash-alt"></i></button>';
								html += '<button class="btn btn-xs btn-primary" id="btnPageEditRow" data-codigo="' + data + '"><i class="fa fa-pencil-alt"></i></button>';
								return html;
							}
						}
						]
			});
			
			
			//CARGAR DATATABLE
	        LoadDataTableWithAjax()
		});

		
		
		//--- BEGIN BUTTON NUEVO ----------------------------------------------------
		$('body').on('click', '#btnPageNewRow', function (e) {
			//REINICIAR FORMULARIO
			$('#formModal')[0].reset();
			$("input[type=hidden]").val('');
			$("#inpModalCodigo").prop('disabled', false);
			
			//CARGAR MODAL
            $('#modal-dialog').modal('show');
		});
					
		//--- BEGIN BUTTON REFRESCAR  ----------------------------------------------------
		$('body').on('click', '#btnPageRefreshRows', function (e) {			
			//CARGAR DATATABLE
	        LoadDataTableWithAjax()
			
		});
		
		//--- BEGIN BUTTON INSERTAR ----------------------------------------------------
		$('body').on('click', '#btnModalSave', function (e) {
			
			var formToJSON =JSON.stringify({
                "emp_codigo": $.trim($("#inpModalCodigo").val()),
			    "emp_nombre": $.trim($("#inpModalNombre").val()),
			    "emp_apellido1": $.trim($("#inpModalApellido1").val()),
			    "emp_apellido2": $.trim($("#inpModalApellido2").val()),
			    "emp_telefono": $.trim($("#inpModalTelefono").val()),
			    "emp_direccion" : $.trim($("#inpModalDireccion").val()),
			 	"emp_estado": $("#inpModalEstado").val()
			  
            });
			
			//INPUT HIDDEN OCULTO 
			var op = $.trim($("#hddModalCodigo").val());
						
			$.ajax({
                type: (op.length == 0 ? "POST" : "PUT"), //SI TIENE ES UNA ACTUALIZACION
                url: "${pageContext.request.contextPath}/Empleado/Rest",
                data: formToJSON,
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
               dataType: "json",
               async: true,
                success: function (response) {
               		//ADD NOTIFICACION
                   	ShowMessage(response.message);
               		
               		//OCULTAR MODAL
                      $("#modal-dialog").modal('hide');
               		
               		if(response.status === true){
                      	//CARGAR DATATABLE
            	        LoadDataTableWithAjax()
               		}
                },
                error: OnErrorCall
            });
			
		});
		
		//--- BEGIN BUTTON ELIMINAR ----------------------------------------------------			
		$('#data-table-default').on('click', '#btnPageDeleteRow', function (e) {
			var object =  $(this);			
			$.ajax({
                type: "DELETE",
                url: "Rest/" + $(this).data('codigo'),
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {
                	//ADD NOTIFICACION
                	ShowMessage(response.message);
                			                		
                    if(response.status === true){
                    	//REMOVE ROW
                    	object.parent().parent().remove();
                    }
                },
                error: OnErrorCall
            });

		});
		
		//--- BEGIN BUTTON EDITAR ----------------------------------------------------
        $('#data-table-default').on('click', '#btnPageEditRow', function (e) {
        	//REINICIAR FORMULARIO
			$('#formModal')[0].reset();
			$("input[type=hidden]").val('');
			$("#inpModalCodigo").prop('disabled', true);
			
        	//AJAX PARA OBTENER UN USUARIO
			$.ajax({
                type: "GET",
                url: "Rest/" + $(this).data('codigo'),
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {
	               	//ADD NOTIFICACION
	               	ShowMessage(response.message);
	              			                		
                     if(response.status === true){
                     	//CARGAR DATA
                     	var d = response.data;	
						$("#inpModalCodigo").val($.trim(d.emp_codigo));
						$("#hddModalCodigo").val($.trim(d.emp_codigo));
						$("#inpModalNombre").val($.trim(d.emp_nombre));
						$("#inpModalApellido1").val($.trim(d.emp_apellido1));
						$("#inpModalApellido2").val($.trim(d.emp_apellido2));
						$("#inpModalTelefono").val($.trim(d.emp_telefono));
						$("#inpModalDireccion").val($.trim(d.emp_direccion));					    		
						$('#inpModalEstado option[value="' + $.trim(d._estado) + '"]').attr("selected", "selected");
                     	//CARGAR MODAL
         	            $('#modal-dialog').modal('show');
                     }
                },
                error: OnErrorCall
            });
        });
		
		
        function OnErrorCall( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
            	console.log('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
            	console.log('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
            	console.log('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
            	console.log('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
            	console.log('Time out error.');
            } else if (textStatus === 'abort') {
            	console.log('Ajax request aborted.');
            } else {
            	console.log('Uncaught Error: ' + jqXHR.responseText);
           }
        }
        
        function LoadDataTableWithAjax(){
        	$('#data-table-default').dataTable().fnClearTable();
			$('#data-table-default').dataTable().fnDraw();
        	$.ajax({
                type: "GET",
                url: "${pageContext.request.contextPath}/Empleado/Rest",
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (response) {                                            
                	//ADD NOTIFICACION
                	ShowMessage(response.message);
                	
                	//UPDATE DATATABLES
                    $('#data-table-default').dataTable().fnAddData(response.data);
                },
                error: OnErrorCall
            });
        }
		
        function ShowMessage(m){
        	//REMOVE NOTIFICACIONES
    		//$.gritter.removeAll();
        	
        	$.gritter.add({
    			title:"Color Admin | Mensaje",
    			text: m,
    			image:"https://seantheme.com/color-admin-v4.0/admin/assets/img/user/user-2.jpg",
    			sticky:!1,
    			time:"",
    			class_name:"my-sticky-class"});
        }
	</script>
  </body> 
</html>