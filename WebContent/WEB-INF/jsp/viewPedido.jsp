<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
	<head>
	<meta charset="utf-8" />
	<title>Color Admin | Clientes</title>
	  	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<jsp:include page="includeBaseCSS.jsp"/> 	
		<!-- ================== END BASE CSS STYLE ================== -->
		
		<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
		
		<!-- ================== BEGIN BASE JS ================== -->
		<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/pace/pace.min.js"></script>
		<!-- ================== END BASE JS ================== -->

	  	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>  
	</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	

<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="index.html" class="navbar-brand"><span class="navbar-logo"></span> <b>Color</b> Admin</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li>
					<form class="navbar-form">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Enter keyword" />
							<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
						</div>
					</form>
				</li>			
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<img src="https://seantheme.com/color-admin-v4.0/admin/assets/img/user/user-13.jpg" alt="" /> 
						<span class="d-none d-md-inline">JORALXJAV</span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="javascript:;" class="dropdown-item">Edit Profile</a>
						<div class="dropdown-divider"></div>
						<a href="javascript:;" class="dropdown-item">Log Out</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="image">
								<img src="https://seantheme.com/color-admin-v4.0/admin/assets/img/user/user-13.jpg" alt="" />
							</div>
							<div class="info">
								<b class="caret pull-right"></b>
								JORALXJAV
								<small>Front end developer</small>
							</div>
						</a>
					</li>
					<li>
						<ul class="nav nav-profile">
                            <li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
                            <li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
                            <li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
                        </ul>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Navigation</li>
					<li class="has-sub">
						<a href="javascript:;">
					        <b class="caret"></b>
						    <i class="fa fa-th-large"></i>
						    <span>Dashboard</span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="index.html">Dashboard v1</a></li>
						    <li><a href="index_v2.html">Dashboard v2</a></li>
						</ul>
					</li>
					<li>
						<a href="bootstrap_4.html">
							<div class="icon-img">
						    	<img src="https://seantheme.com/color-admin-v4.0/admin/assets/img/logo/logo-bs4.png" alt="" />
						    </div>
						    <span>Bootstrap 4 <span class="label label-theme m-l-5">NEW</span></span> 
						</a>
					</li>
					<li class="has-sub active">
						<a href="javascript:;">
					        <b class="caret"></b>
						    <i class="fa fa-table"></i>
						    <span>Mantenimiento</span>
						</a>
						<ul class="sub-menu">
							<li><a href="../Usuario/Web">Cliente <i class="fa fa-paper-plane text-theme m-l-5"></i></a></li>
							<li><a href="../Linea/Web">Lineas </a></li>	
							<li><a href="../Articulo/Web">Articulos </a></li>								
						</ul>
					</li>

			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item"><a href="javascript:;">Mantenimiento</a></li>
				<li class="breadcrumb-item active">Cliente</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Cliente <small>Mantenimiento de Cliente...</small></h1>
			<!-- end page-header -->
			
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<div class="panel-heading-btn">						
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" id="btnPageNewRow"><i class="fa fa-plus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" id="btnPageRefreshRows"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="Xls" class="btn btn-xs btn-icon btn-circle btn-primary"><i class="fa fa-file-excel"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Data Table - Default</h4>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">


<table id="data-table-default" class="table table-striped table-bordered">
						<thead>							
							<th></th>
							<th>Codigo</th>							
							<th>tpedido&oacute;n</th>							
							<th>npedido</th>
							<th>descripcion</th>
							<th></th>
						</thead>
						<tbody>
										                 
							<c:forEach var="item" items="${listadoPedido}" varStatus="status">
							<tr>
							    <td>${status.index + 1}</td>
							    <td>${item.ped_codigo}</td>							    
							    <td>${item.ped_tpedido}</td>
							    <td>${item.ped_npedido}</td>
							    <td>${item.ped_descripcion}</td>
							    <td></td>
							    <td>
							    	<button class="btn btn-xs btn-danger" id="btnPageDeleteRow"><i class="fa fa-trash-alt"></i></button>
							    	<button class="btn btn-xs btn-primary" id="btnPageEditRow"><i class="fa fa-pencil-alt"></i></button>
							    </td>
							 </tr>
							 </c:forEach>
					
						</tbody>
					</table>
					<!-- FIN S502CI -->
					
					
		<!-- FIN S502CI -->
					
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end #content -->

		<!-- #modal-dialog -->
		<div class="modal fade" id="modal-dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Usuario</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						    <div class="alert alert-secondary">
                        		Porfavor ingrese todos los <code>&lt;input&gt;</code>s, necesarios.
                        	</div>
                        	<form id="formModal">
                        		<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">C&oacute;digo</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalCodigo" placeholder="Codigo" />
										<input type="hidden" id="hddModalCodigo"/>
										<small class="f-s-12 text-grey-darker">Automatico.</small>
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Nombre</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalNombre" placeholder="Nombre Usuario" />
										<small class="f-s-12 text-grey-darker">Prefijo:[1 caracter del Nombre]+[Apellido Paterno].</small>
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Contraseña</label>
									<div class="col-md-9">
										<input type="password" class="form-control m-b-5"  id="inpModalPasswd" placeholder="Contraseña" />
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Descripción</label>
									<div class="col-md-9">
										<input type="text" class="form-control m-b-5"  id="inpModalDescri" placeholder="Nombre completo" />
									</div>
								</div>								
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Correo</label>
									<div class="col-md-9">
										<input type="email" class="form-control m-b-5"  id="inpModalEmail" placeholder="Correo" />
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Foto</label>
									<div class="col-md-9">
										<input type="file" class="form-control m-b-5"  id="inpModalImage" placeholder="Foto" />
										<small class="f-s-12 text-grey-darker">200px X 200px.</small>
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-md-3">Estado:</label>
									<div class="col-md-9">
										<select class="form-control" id="inpModalEstado">											
										</select>
									</div>
								</div>
							</form>
					</div>
					<div class="modal-footer">
						<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
						<a href="javascript:;" class="btn btn-success" id="btnModalSave">Guardar</a>
					</div>
				</div>
			</div>
		</div>
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<jsp:include page="includeBaseJS.jsp"/>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="https://seantheme.com/color-admin-v4.0/admin/assets/js/demo/table-manage-default.demo.min.js"></script>	
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		
	
		$(document).ready(function() {
			
			App.init();
			TableManageDefault.init();
			
		});

		
		//--- BEGIN BUTTON EDITAR ----------------------------------------------------
        $('#data-table-default').on('click', '#btnPageEditRow', function (e) {
        		
        		//CARGAR MODAL
	        $('#modal-dialog').modal('show');
	
        });
		

		//--- FUNCTION CONTROL ERROR ----------------------------------------------------
        function OnErrorCall( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
            	console.log('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
            	console.log('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
            	console.log('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
            	console.log('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
            	console.log('Time out error.');
            } else if (textStatus === 'abort') {
            	console.log('Ajax request aborted.');
            } else {
            	console.log('Uncaught Error: ' + jqXHR.responseText);
           }
        }


		//--- BEGIN MOSTRAR MENSAJE ----------------------------------------------------
        function ShowMessage(m){
        	
	        	$.gritter.add({
	    			title:"Color Admin | Mensaje",
	    			text: m,
	    			image:"https://seantheme.com/color-admin-v4.0/admin/assets/img/user/user-2.jpg",
	    			sticky:!1,
	    			time:"",
	    			class_name:"my-sticky-class"});
        }
	</script>
  </body> 
</html>