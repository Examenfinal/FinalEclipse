

CREATE TABLE pizmaecliente
(
	cli_codigo           CHAR(18) NOT NULL,
	cli_nombre           varchar(50) NOT NULL,
	cli_apellido         CHAR(18) NULL,
	cli_telefono         char(10) NOT NULL,
	cli_direccion        varchar(100) NULL,
	cli_estado           integer
);



ALTER TABLE pizmaecliente
ADD PRIMARY KEY (cli_codigo);



CREATE TABLE pizmaeemplado
(
	emp_codigo           CHAR(18) NOT NULL,
	emp_nombre           varchar(50) NOT NULL,
	emp_apellido1        varchar(50) NOT NULL,
	emp_apellido2        varchar(50) NOT NULL,
	emp_telefono         char(10) NOT NULL,
	emp_direccion        varchar(100) NOT NULL,
	emp_estado           integer
);



ALTER TABLE pizmaeemplado
ADD PRIMARY KEY (emp_codigo);



CREATE TABLE pizmaepedido
(
	ped_codigo           char(18) NOT NULL,
	ped_tpedido          varchar(50) NOT NULL,
	ped_npedido          INTEGER NOT NULL,
	ped_descripcion      varchar(50) NULL
);



ALTER TABLE pizmaepedido
ADD PRIMARY KEY (ped_codigo);



CREATE TABLE pizmaeproducto
(
	pro_codigo           CHAR(18) NOT NULL,
	pro_nombre           varchar(50) NOT NULL,
	pro_precio           decimal NOT NULL,
	pro_marca            varchar(50) NOT NULL,
	pro_categoria        varchar(50) NOT NULL,
	pro_estado           integer
);



ALTER TABLE pizmaeproducto
ADD PRIMARY KEY (pro_codigo);



CREATE TABLE pizmaeproveedor
(
	prv_codigo           CHAR(18) NOT NULL,
	prv_rsocial          varchar(50) NOT NULL,
	prv_telefono         char(10) NOT NULL,
	prv_estado           INTEGER NOT NULL
);



ALTER TABLE pizmaeproveedor
ADD PRIMARY KEY (prv_codigo);









