package pe.edu.idat.pizzeria.model;


import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;


public abstract class EmpleadoModel {
	
	
	private static final long serialVersionUID = 1L;
	
	String emp_codigo;
	String emp_nombre;
	String emp_apellido1;
	String emp_apellido2;
	String emp_telefono;
	String emp_direccion;
	int emp_estado;
	
	

	public int getEmp_estado() {
		return emp_estado;
	}
	public void setEmp_estado(int emp_estado) {
		this.emp_estado = emp_estado;
	}
	public String getEmp_codigo() {
		return emp_codigo;
	}
	public void setEmp_codigo(String emp_codigo) {
		this.emp_codigo = emp_codigo;
	}
	public String getEmp_nombre() {
		return emp_nombre;
	}
	public void setEmp_nombre(String emp_nombre) {
		this.emp_nombre = emp_nombre;
	}
	public String getEmp_apellido1() {
		return emp_apellido1;
	}
	public void setEmp_apellido1(String emp_apellido1) {
		this.emp_apellido1 = emp_apellido1;
	}
	public String getEmp_apellido2() {
		return emp_apellido2;
	}
	public void setEmp_apellido2(String emp_apellido2) {
		this.emp_apellido2 = emp_apellido2;
	}
	public String getEmp_telefono() {
		return emp_telefono;
	}
	public void setEmp_telefono(String emp_telefono) {
		this.emp_telefono = emp_telefono;
	}
	public String getEmp_direccion() {
		return emp_direccion;
	}
	public void setEmp_direccion(String emp_direccion) {
		this.emp_direccion = emp_direccion;
	}
	
	
	
}
