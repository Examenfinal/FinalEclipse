package pe.edu.idat.pizzeria.model;


import java.io.Serializable;
import java.util.Date;

public abstract  class ParametroModel implements Serializable {

	String cli_codigo;
	String cli_tipcod;
	String cli_argume;
	String cli_funcion;
	String cliu_usucod;
	Date cli_fecreg;
	public String getCli_codigo() {
		return cli_codigo;
	}
	public void setCli_codigo(String cli_codigo) {
		this.cli_codigo = cli_codigo;
	}
	public String getCli_tipcod() {
		return cli_tipcod;
	}
	public void setCli_tipcod(String cli_tipcod) {
		this.cli_tipcod = cli_tipcod;
	}
	public String getCli_argume() {
		return cli_argume;
	}
	public void setCli_argume(String cli_argume) {
		this.cli_argume = cli_argume;
	}
	public String getCli_funcion() {
		return cli_funcion;
	}
	public void setCli_funcion(String cli_funcion) {
		this.cli_funcion = cli_funcion;
	}
	public String getCliu_usucod() {
		return cliu_usucod;
	}
	public void setCliu_usucod(String cliu_usucod) {
		this.cliu_usucod = cliu_usucod;
	}
	public Date getCli_fecreg() {
		return cli_fecreg;
	}
	public void setCli_fecreg(Date cli_fecreg) {
		this.cli_fecreg = cli_fecreg;
	}
	
	
	
	
}
