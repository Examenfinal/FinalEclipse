package pe.edu.idat.pizzeria.model;
import java.io.Serializable;


public abstract class PedidoModel  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	String ped_codigo;
	String ped_tpedido;
	int ped_npedido;
	String ped_descripcion;
	
	
	

	public String getPed_codigo() {
		return ped_codigo;
	}
	public void setPed_codigo(String ped_codigo) {
		this.ped_codigo = ped_codigo;
	}
	public String getPed_tpedido() {
		return ped_tpedido;
	}
	public void setPed_tpedido(String ped_tpedido) {
		this.ped_tpedido = ped_tpedido;
	}
	
	
	public int getPed_npedido() {
		return ped_npedido;
	}
	public void setPed_npedido(int ped_npedido) {
		this.ped_npedido = ped_npedido;
	}
	public String getPed_descripcion() {
		return ped_descripcion;
	}
	public void setPed_descripcion(String ped_descripcion) {
		this.ped_descripcion = ped_descripcion;
	}
	
	
	
	

}
