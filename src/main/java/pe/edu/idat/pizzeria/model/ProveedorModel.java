package pe.edu.idat.pizzeria.model;

public abstract class ProveedorModel {

	

	String prv_codigo;
	String prv_rsocial;
	String prv_telefono;
	
	int prv_estado;
	
	
	
	
	public String getPrv_codigo() {
		return prv_codigo;
	}
	public void setPrv_codigo(String prv_codigo) {
		this.prv_codigo = prv_codigo;
	}
	public String getPrv_rsocial() {
		return prv_rsocial;
	}
	public void setPrv_rsocial(String prv_rsocial) {
		this.prv_rsocial = prv_rsocial;
	}
	public String getPrv_telefono() {
		return prv_telefono;
	}
	public void setPrv_telefono(String prv_telefono) {
		this.prv_telefono = prv_telefono;
	}
	public int getPrv_estado() {
		return prv_estado;
	}
	public void setPrv_estado(int prv_estado) {
		this.prv_estado = prv_estado;
	}

	
	
	
}
