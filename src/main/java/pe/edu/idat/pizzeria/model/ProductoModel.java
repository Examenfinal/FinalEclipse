package pe.edu.idat.pizzeria.model;

public abstract class ProductoModel {

	
	String pro_codigo;
	String pro_nombre;
	String pro_precio;
	String pro_marca;
	String pro_categoria;
	int pro_estado;
	
	
	
	
	public int getPro_estado() {
		return pro_estado;
	}
	public void setPro_estado(int pro_estado) {
		this.pro_estado = pro_estado;
	}
	public String getPro_codigo() {
		return pro_codigo;
	}
	public void setPro_codigo(String pro_codigo) {
		this.pro_codigo = pro_codigo;
	}
	public String getPro_nombre() {
		return pro_nombre;
	}
	public void setPro_nombre(String pro_nombre) {
		this.pro_nombre = pro_nombre;
	}
	public String getPro_precio() {
		return pro_precio;
	}
	public void setPro_precio(String pro_precio) {
		this.pro_precio = pro_precio;
	}
	public String getPro_marca() {
		return pro_marca;
	}
	public void setPro_marca(String pro_marca) {
		this.pro_marca = pro_marca;
	}
	public String getPro_categoria() {
		return pro_categoria;
	}
	public void setPro_categoria(String pro_categoria) {
		this.pro_categoria = pro_categoria;
	}

	
	
	
}
