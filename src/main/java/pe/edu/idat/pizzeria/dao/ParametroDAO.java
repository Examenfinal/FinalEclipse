package pe.edu.idat.pizzeria.dao;


import java.util.List;
import java.util.Map;

import pe.edu.idat.pizzeria.bean.ParametroBean;

public interface ParametroDAO {


	public List<ParametroBean> selParametro(Map<String, Object> parametros) throws Exception;
	}