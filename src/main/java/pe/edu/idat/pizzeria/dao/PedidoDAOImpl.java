package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;


import pe.edu.idat.pizzeria.bean.PedidoBean;
//import pe.edu.idat.pizzeria.dao.PedidoDAOImpl;

/*public class PedidoDAOImpl implements PedidoDAO{

	// DECLARACIONES DAO
		static Log log = LogFactory.getLog(PedidoDAOImpl.class.getName());	
		private JdbcTemplate jdbcTemplate;
	    private int rows;
	    private PedidoBean pedidoBean = null;
	    
		// JDBCTEMPLATE
	    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	 @SuppressWarnings({ "unchecked", "rawtypes" })
	  
	
	// LISTAR X CODIGO
	 
	@Override
	public PedidoBean selPedidoByCodigo(String pedCodigo)  throws Exception{
        if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.selPedidoByCodigo");
        
        String SQL = "SELECT * FROM pizmaepedido WHERE ped_codigo = ? ";        
		try {
			pedidoBean = (PedidoBean) jdbcTemplate.queryForObject(SQL, new Object[] { pedCodigo }, new RowMapper(){
			            @Override
			            public PedidoBean mapRow(ResultSet rs, int rowNum) throws SQLException
			            {
			            	PedidoBean u = new PedidoBean();
			            	u.setPed_codigo(rs.getString("ped_codigo"));	                	
		                	u.setPed_tpedido(rs.getString("ped_tpedido"));
		                	u.setPed_npedido(rs.getInt("ped_npedido"));
		                
		                	u.setPed_descripcion(rs.getString("ped_descripcion"));
		                		
			                return u;
			            }
			        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidDAOImpl.selPedidByCodigo");
			log.error(ex, ex);
			throw ex;	
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - PedidDAOImpl.selPedidByCodigo");
		}
		
		if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.selectPedidByCodigo");
		
		return pedidoBean;
	}
	
	
	// LISTAR 
	    @Override
		public List<PedidoBean> SelPedido() throws Exception{		
			if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.selectPedido");
			
			List<PedidoBean> lstCliente = null;
			
			String SQL ="SELECT * FROM pizmaepedido";
			//call prcClienteSelect
			
			try {		
				lstCliente = jdbcTemplate.query(SQL, new ResultSetExtractor<List<PedidoBean>>(){
		            @Override
		            public List<PedidoBean> extractData(ResultSet rs) throws SQLException, DataAccessException
		            {
		            	List<PedidoBean> list = new ArrayList<PedidoBean>();
		                while (rs.next())
		                {
		                	/*PedidoBean u = new PedidoBean();
		                	u.setCli_codigo(rs.getString("cli_codigo"));	                	
		                	u.setCli_nombre(rs.getString("cli_nombre"));
		                	u.setCli_apellido(rs.getString("cli_apellido"));
		                	u.setCli_telefono(rs.getString("cli_telefono"));
		                	u.setCli_direccion(rs.getString("cli_direccion"));
		                	u.setCli_estado(rs.getInt("cli_estado"));		                	
		                    list.add(u);
		                    
		                }
		                return list;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.selectCliente");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selectCliente");
			}
		    return lstCliente;
		}
	
	
	    
	    
	 // INSERTAR
		@Override
		public boolean insPedido(PedidoBean pedido) throws Exception {		
	        if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.insCliente");
	        StringBuilder SQL = new StringBuilder(); 
	        SQL.append("INSERT INTO pizmaecliente");
	        SQL.append("(cli_codigo, cli_nombre, cli_apellido, cli_telefono, cli_direccion,cli_estado) ");
	        SQL.append("VALUES(?,?,?,?,?,?)");
	        
	        //String SQL ="call prcClienteInsert(?,?,?,?,?,?)";
			try {			
				rows = jdbcTemplate.update(SQL.toString(), new Object[] {
						
						/*cliente.getCli_codigo(),
						 cliente.getCli_nombre(),
						 cliente.getCli_apellido(),
						 cliente.getCli_telefono(),
						 cliente.getCli_direccion(),
						 cliente.getCli_estado()});
											 
			
			} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.insCliente");
					log.error(ex, ex);
					throw ex;
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.insCliente");
			}
			return  rows ==1;
			
		}
		
		//ACTUALIZAR
		@Override
		public boolean updPedido(PedidoBean pedido) throws Exception {		
	       if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.updCliente");		
			String SQL = "UPDATE pizmaecliente SET cli_nombre =?, cli_apellido =?, cli_telefono =?, cli_direccion =?, cli_estado =?  WHERE cli_codigo =?";
			/*
			try {
				rows = jdbcTemplate.update(SQL, new Object[] {pedido.getCli_nombre(),
						 cliente.getCli_apellido(),
						 cliente.getCli_telefono(),
						 cliente.getCli_direccion(),
						 cliente.getCli_estado(),
						 cliente.getCli_codigo() 
						 });
				
				
			} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.updCliente");
					log.error(ex, ex);
					throw ex;
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.updCliente");
			}
			return  rows ==1;
			
			
		}
		
		
		
		
		// ELIMINAR
		@Override
		public boolean delCliente(String cliCodigo) throws Exception {		
			if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.delCliente");		
			String SQL = "DELETE FROM pizmaecliente WHERE cli_codigo = ? ";		
			try {			
				rows = jdbcTemplate.update(SQL, new Object[] { cliCodigo });
			} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.delCliente");
					log.error(ex, ex);
					throw ex;
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.delCliente");
			}	     
			return rows == 1;
		}


}
*/