package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import pe.edu.idat.pizzeria.dao.ClienteDAOImpl;

import pe.edu.idat.pizzeria.bean.ClienteBean;

public class ClienteDAOImpl  implements ClienteDAO{
	
	
	// DECLARACIONES DAO
		static Log log = LogFactory.getLog(ClienteDAOImpl.class.getName());	
		private JdbcTemplate jdbcTemplate;
	    private int rows;
	    private ClienteBean clienteBean = null;
	    
		// JDBCTEMPLATE
	    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	 @SuppressWarnings({ "unchecked", "rawtypes" })
	  
	
	// LISTAR X CODIGO
	 
	@Override
	public ClienteBean selClienteByCodigo(String cliCodigo)  throws Exception{
        if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.selClienteByCodigo");
        
        String SQL = "SELECT * FROM pizmaecliente WHERE cli_codigo = ? ";        
		try {
			clienteBean = (ClienteBean) jdbcTemplate.queryForObject(SQL, new Object[] { cliCodigo }, new RowMapper(){
			            @Override
			            public ClienteBean mapRow(ResultSet rs, int rowNum) throws SQLException
			            {
			            	ClienteBean u = new ClienteBean();
			            	u.setCli_codigo(rs.getString("cli_codigo"));	                	
		                	u.setCli_nombre(rs.getString("cli_nombre"));
		                	u.setCli_apellido(rs.getString("cli_apellido"));
		                	u.setCli_telefono(rs.getString("cli_telefono"));
		                	u.setCli_direccion(rs.getString("cli_direccion"));
		                	u.setCli_estado(rs.getInt("cli_estado"));	
			                return u;
			            }
			        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.selClienteByCodigo");
			log.error(ex, ex);
			throw ex;	
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selClienteByCodigo");
		}
		
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.selectClienteByCodigo");
		
		return clienteBean;
	}
	
	
	// LISTAR 
	    @Override
		public List<ClienteBean> SelCliente() throws Exception{		
			if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.selectCliente");
			
			List<ClienteBean> lstCliente = null;
			
			String SQL ="SELECT * FROM pizmaecliente";
			//call prcClienteSelect
			
			try {		
				lstCliente = jdbcTemplate.query(SQL, new ResultSetExtractor<List<ClienteBean>>(){
		            @Override
		            public List<ClienteBean> extractData(ResultSet rs) throws SQLException, DataAccessException
		            {
		            	List<ClienteBean> list = new ArrayList<ClienteBean>();
		                while (rs.next())
		                {
		                	ClienteBean u = new ClienteBean();
		                	u.setCli_codigo(rs.getString("cli_codigo"));	                	
		                	u.setCli_nombre(rs.getString("cli_nombre"));
		                	u.setCli_apellido(rs.getString("cli_apellido"));
		                	u.setCli_telefono(rs.getString("cli_telefono"));
		                	u.setCli_direccion(rs.getString("cli_direccion"));
		                	u.setCli_estado(rs.getInt("cli_estado"));		                	
		                    list.add(u);
		                }
		                return list;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.selectCliente");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selectCliente");
			}
		    return lstCliente;
		}
	
	
	    
	    
	 // INSERTAR
		@Override
		public boolean insCliente(ClienteBean cliente) throws Exception {		
	        if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.insCliente");
	        StringBuilder SQL = new StringBuilder(); 
	        SQL.append("INSERT INTO pizmaecliente");
	        SQL.append("(cli_codigo, cli_nombre, cli_apellido, cli_telefono, cli_direccion,cli_estado) ");
	        SQL.append("VALUES(?,?,?,?,?,?)");
	        
	        //String SQL ="call prcClienteInsert(?,?,?,?,?,?)";
			try {			
				rows = jdbcTemplate.update(SQL.toString(), new Object[] {cliente.getCli_codigo(),
						 cliente.getCli_nombre(),
						 cliente.getCli_apellido(),
						 cliente.getCli_telefono(),
						 cliente.getCli_direccion(),
						 cliente.getCli_estado()});
											 
			
			} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.insCliente");
					log.error(ex, ex);
					throw ex;
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.insCliente");
			}
			return  rows ==1;
		}
		
		//ACTUALIZAR
		@Override
		public boolean updCliente(ClienteBean cliente) throws Exception {		
	        if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.updCliente");		
			String SQL = "UPDATE pizmaecliente SET cli_nombre =?, cli_apellido =?, cli_telefono =?, cli_direccion =?, cli_estado =?  WHERE cli_codigo =?";
			try {
				rows = jdbcTemplate.update(SQL, new Object[] {cliente.getCli_nombre(),
						 cliente.getCli_apellido(),
						 cliente.getCli_telefono(),
						 cliente.getCli_direccion(),
						 cliente.getCli_estado(),
						 cliente.getCli_codigo() 
						 });
				
				
			} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.updCliente");
					log.error(ex, ex);
					throw ex;
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.updCliente");
			}
			return  rows ==1;
		}
		
		
		
		
		// ELIMINAR
		@Override
		public boolean delCliente(String cliCodigo) throws Exception {		
			if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.delCliente");		
			String SQL = "DELETE FROM pizmaecliente WHERE cli_codigo = ? ";		
			try {			
				rows = jdbcTemplate.update(SQL, new Object[] { cliCodigo });
			} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.delCliente");
					log.error(ex, ex);
					throw ex;
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.delCliente");
			}	     
			return rows == 1;
		}

		
		
		/*@Override
		public ClienteBean selClienteByCodigoAndPassword(String cliNombre,String cliPassword) throws Exception {		
			if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.selClienteByCodigoAndPassword");		
			
			try {					
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource()).withProcedureName("prcClienteLogin");
				 	MapSqlParameterSource params = new MapSqlParameterSource();
				 	params.addValue("in_nombre", cliNombre);
		            params.addValue("in_passwd", cliPassword);
		            
				    Map<String, Object> out = jdbcCall.execute(params);
				    
				    if(out.get("out_codigo") != null){				   
				    	clienteBean= new ClienteBean();
				    	clienteBean.setCli_codigo((String) out.get("out_codigo"));
				    	clienteBean.setCli_nombre(cliNombre);
				    	clienteBean.setCli_apellido((String)out.get("out_apellido"));
				    	
				    	clienteBean.setCli_telefono((String)out.get("out_telefono")  );
				    	//clienteBean.setUsu_passwd(usuPassword);
				    	clienteBean.setCli_direccion((String) out.get("out_direccion"));
				    	
				    	clienteBean.setCli_estado((Integer) out.get("out_estado"));
				    }
			} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.selClienteByCodigoAndPassword");
					log.error(ex, ex);
					throw ex;
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selClienteByCodigoAndPassword");
			}	     
			return clienteBean;
		}
		
		
		
	public List<ClienteBean> SelCliente() {
		  @SuppressWarnings({ "unchecked", "rawtypes" })
		  
		 
		  
		  
		List<ClienteBean> lst = null;
		String sql ="SELECT * FROM pizmaecliente";
		
			
			lst = jdbcTemplate.query(sql, new ResultSetExtractor<List<ClienteBean>>(){
	            @Override
	            public List<ClienteBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<ClienteBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	ClienteBean i = new ClienteBean();
	                	i.setCli_codigo(rs.getString("cli_codigo"));	                	
	                	i.setCli_nombre(rs.getString("cli_nombre"));
	                	i.setCli_direccion(rs.getString("cli_direccion"));
	                	i.setCli_estado(rs.getInt("cli_estado"));		                	
	                    list.add(i);
	                }
	                return list;
	            }

	        });	    
		
		
	    return lst;
		
	
	}
	
	*/
}
