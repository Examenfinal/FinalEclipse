package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.pizzeria.bean.ProveedorBean;

public class ProveedorDAOImpl  implements ProveedorDAO{
private JdbcTemplate jdbcTemplate;
	
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	@Override
	public List<ProveedorBean> SelProveedor() {

		List<ProveedorBean> lst = null;
		String sql ="SELECT * FROM pizmaeproveedor";
		
			
			lst = jdbcTemplate.query(sql, new ResultSetExtractor<List<ProveedorBean>>(){
	            @Override
	            public List<ProveedorBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<ProveedorBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	ProveedorBean i = new ProveedorBean();
	                	i.setPrv_codigo(rs.getString("prv_codigo"));	                	
	                	i.setPrv_rsocial(rs.getString("prv_rsocial"));
	                	i.setPrv_telefono(rs.getString("prv_telefono"));
	                	i.setPrv_estado(rs.getInt("prv_estado"));		                	
	                    list.add(i);
	                }
	                return list;
	            }

	        });	    
		
		
	    return lst;
		
		
	}

}
