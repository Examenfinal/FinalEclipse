package pe.edu.idat.pizzeria.dao;

import java.util.List;


import pe.edu.idat.pizzeria.bean.PedidoBean;

public interface PedidoDAO {
	
	 public PedidoBean selPedidoByCodigo(String pedCodigo) throws Exception;
     
	    public List<PedidoBean> SelPedido() throws Exception;
	    
	    public boolean insPedido(PedidoBean pedido)throws Exception;
	    
	    public boolean updPedido(PedidoBean pedido)throws Exception;
	    
	    public boolean delPedido(String pedCodigo)throws Exception; 
	    
	    /*public ClienteBean selClienteByCodigoAndPassword(String cliCodigo,String cliPassword) throws Exception;*/
	}


