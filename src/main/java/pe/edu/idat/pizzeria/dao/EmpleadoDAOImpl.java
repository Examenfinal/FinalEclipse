package pe.edu.idat.pizzeria.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;


import pe.edu.idat.pizzeria.bean.EmpleadoBean;

public class EmpleadoDAOImpl implements EmpleadoDAO{

	// DECLARACIONES DAO
			static Log log = LogFactory.getLog(EmpleadoDAOImpl.class.getName());	
			private JdbcTemplate jdbcTemplate;
		    private int rows;
		    private EmpleadoBean empleadoBean = null;
		    
			// JDBCTEMPLATE
		    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
		}
		 @SuppressWarnings({ "unchecked", "rawtypes" })
		  
		
		// LISTAR X CODIGO
		 
		@Override
		public EmpleadoBean selEmpleadoByCodigo(String empCodigo)  throws Exception{
	        if (log.isDebugEnabled()) log.debug("Inicio - EmpleadoDAOImpl.selEmpleadoByCodigo");
	        
	        String SQL = "SELECT * FROM pizmaeemplado WHERE emp_codigo = ? ";        
			try {
				empleadoBean = (EmpleadoBean) jdbcTemplate.queryForObject(SQL, new Object[] { empCodigo }, new RowMapper(){
				            @Override
				            public EmpleadoBean mapRow(ResultSet rs, int rowNum) throws SQLException
				            {
				            	EmpleadoBean u = new EmpleadoBean();
				            	u.setEmp_codigo(rs.getString("emp_codigo"));	                	
			                	u.setEmp_nombre(rs.getString("emp_nombre"));
			                	u.setEmp_apellido1(rs.getString("emp_apellido1"));
			                	u.setEmp_apellido2(rs.getString("emp_apellido2"));
			                	u.setEmp_telefono(rs.getString("emp_telefono"));
			                	u.setEmp_direccion(rs.getString("emp_direccion"));
			                	u.setEmp_estado(rs.getInt("emp_estado"));	
				                return u;
				            }
				        });
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - EmpleadoDAOImpl.selEmpleadoByCodigo");
				log.error(ex, ex);
				throw ex;	
			} finally {
				if (log.isDebugEnabled()) log.debug("Final - EmpleadoDAOImpl.selEmpleadoByCodigo");
			}
			
			if (log.isDebugEnabled()) log.debug("Inicio - EmpleadoDAOImpl.selectEmpleadoByCodigo");
			
			return empleadoBean;
		}
		
		
		// LISTAR 
		    @Override
			public List<EmpleadoBean> SelEmpleado() throws Exception{		
				if (log.isDebugEnabled()) log.debug("Inicio - EmpleadoDAOImpl.selectEmpleado");
				
				List<EmpleadoBean> lstEmpleado = null;
				
				String SQL ="SELECT * FROM pizmaeemplado";
				//call prcClienteSelect
				
				try {		
					lstEmpleado = jdbcTemplate.query(SQL, new ResultSetExtractor<List<EmpleadoBean>>(){
			            @Override
			            public List<EmpleadoBean> extractData(ResultSet rs) throws SQLException, DataAccessException
			            {
			            	List<EmpleadoBean> list = new ArrayList<EmpleadoBean>();
			                while (rs.next())
			                {
			                	EmpleadoBean u = new EmpleadoBean();
			                	u.setEmp_codigo(rs.getString("emp_codigo"));	                	
			                	u.setEmp_nombre(rs.getString("emp_nombre"));
			                	u.setEmp_apellido1(rs.getString("emp_apellido1"));
			                	u.setEmp_apellido2(rs.getString("emp_apellido2"));
			                	u.setEmp_telefono(rs.getString("emp_telefono"));
			                	u.setEmp_direccion(rs.getString("emp_direccion"));
			                	u.setEmp_estado(rs.getInt("emp_estado"));		                	
			                    list.add(u);
			                }
			                return list;
			            }

			        });	    
				
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error -EmpleadoDAOImpl.selecEmpleado");
					log.error(ex, ex);
					throw ex;
				}finally{
					if (log.isDebugEnabled()) log.debug("Final - EmpleadoDAOImpl.selectEmpleado");
				}
			    return lstEmpleado;
			}
		
		
		    
		    
		 // INSERTAR
			@Override
			public boolean insEmpleado(EmpleadoBean empleado) throws Exception {		
		        if (log.isDebugEnabled()) log.debug("Inicio - EmpleadoDAOImpl.insEmpleado");
		        //StringBuilder SQL = new StringBuilder(); 
		        //SQL.append("INSERT INTO pizmaeemplado");
		        //SQL.append("(emp_codigo, emp_nombre, emp_apellido1, emp_apellido2, emp_telefono, emp_direccion,emp_estado) ");
		        //SQL.append("VALUES(?,?,?,?,?,?,?)");
		        
		        String SQL ="call prcEmpleadoInsert(?,?,?,?,?,?,?)";
				try {			
					rows = jdbcTemplate.update(SQL.toString(), new Object[] {empleado.getEmp_codigo(),
							empleado.getEmp_nombre(),
							empleado.getEmp_apellido1(),
							empleado.getEmp_apellido2(),
							empleado.getEmp_telefono(),
							empleado.getEmp_direccion(),
							empleado.getEmp_estado()});
												 
				
				} catch (Exception ex) {			
						if (log.isDebugEnabled()) log.debug("Error - EmpleadoDAOImpl.insEmpleado");
						log.error(ex, ex);
						throw ex;
				} finally {
					if (log.isDebugEnabled()) log.debug("Final - EmpleadoDAOImpl.insEmpleado");
				}
				return  rows ==1;
			}
			
			//ACTUALIZAR
			@Override
			public boolean updEmpleado(EmpleadoBean empleado) throws Exception {		
		        if (log.isDebugEnabled()) log.debug("Inicio - EmpleadoDAOImpl.updEmpleado");		
				String SQL = "UPDATE pizmaeemplado SET emp_nombre =?, emp_apellido1 =?, emp_apellido2 =?,emp_telefono =?, emp_direccion =?,emp_estado =?  WHERE emp_codigo =?";
				try {
					rows = jdbcTemplate.update(SQL, new Object[] {empleado.getEmp_nombre(),
							empleado.getEmp_apellido1(),
							empleado.getEmp_apellido2(),
							empleado.getEmp_telefono(),
							empleado.getEmp_direccion(),
							empleado.getEmp_estado(),
							empleado.getEmp_codigo(),
							 });
					
					
				} catch (Exception ex) {			
						if (log.isDebugEnabled()) log.debug("Error - EmpleadoDAOImpl.updEmpleado");
						log.error(ex, ex);
						throw ex;
				} finally {
					if (log.isDebugEnabled()) log.debug("Final - EmpleadoDAOImpl.updEmpleado");
				}
				return  rows ==1;
			}
			
			
			
			
			// ELIMINAR
			@Override
			public boolean delEmpleado(String empCodigo) throws Exception {		
				if (log.isDebugEnabled()) log.debug("Inicio - EmpleadoDAOImpl.delEmpleado");		
				String SQL = "DELETE FROM pizmaeemplado WHERE emp_codigo = ? ";		
				try {			
					rows = jdbcTemplate.update(SQL, new Object[] { empCodigo });
				} catch (Exception ex) {			
						if (log.isDebugEnabled()) log.debug("Error -EmpleadoDAOImpl.delEmpleado");
						log.error(ex, ex);
						throw ex;
				} finally {
					if (log.isDebugEnabled()) log.debug("Final - EmpleadoDAOImpl.delEmpleado");
				}	     
				return rows == 1;
			}

			
			
			/*@Override
			public ClienteBean selClienteByCodigoAndPassword(String cliNombre,String cliPassword) throws Exception {		
				if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.selClienteByCodigoAndPassword");		
				
				try {					
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource()).withProcedureName("prcClienteLogin");
					 	MapSqlParameterSource params = new MapSqlParameterSource();
					 	params.addValue("in_nombre", cliNombre);
			            params.addValue("in_passwd", cliPassword);
			            
					    Map<String, Object> out = jdbcCall.execute(params);
					    
					    if(out.get("out_codigo") != null){				   
					    	clienteBean= new ClienteBean();
					    	clienteBean.setCli_codigo((String) out.get("out_codigo"));
					    	clienteBean.setCli_nombre(cliNombre);
					    	clienteBean.setCli_apellido((String)out.get("out_apellido"));
					    	
					    	clienteBean.setCli_telefono((String)out.get("out_telefono")  );
					    	//clienteBean.setUsu_passwd(usuPassword);
					    	clienteBean.setCli_direccion((String) out.get("out_direccion"));
					    	
					    	clienteBean.setCli_estado((Integer) out.get("out_estado"));
					    }
				} catch (Exception ex) {			
						if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.selClienteByCodigoAndPassword");
						log.error(ex, ex);
						throw ex;
				} finally {
					if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selClienteByCodigoAndPassword");
				}	     
				return clienteBean;
			}
			
			
			
		public List<ClienteBean> SelCliente() {
			  @SuppressWarnings({ "unchecked", "rawtypes" })
			  
			 
			  
			  
			List<ClienteBean> lst = null;
			String sql ="SELECT * FROM pizmaecliente";
			
				
				lst = jdbcTemplate.query(sql, new ResultSetExtractor<List<ClienteBean>>(){
		            @Override
		            public List<ClienteBean> extractData(ResultSet rs) throws SQLException
		            {
		            	List<ClienteBean> list = new ArrayList<>();
		                while (rs.next())
		                {
		                	ClienteBean i = new ClienteBean();
		                	i.setCli_codigo(rs.getString("cli_codigo"));	                	
		                	i.setCli_nombre(rs.getString("cli_nombre"));
		                	i.setCli_direccion(rs.getString("cli_direccion"));
		                	i.setCli_estado(rs.getInt("cli_estado"));		                	
		                    list.add(i);
		                }
		                return list;
		            }

		        });	    
			
			
		    return lst;
			
		
		}
		
		*/
	}
