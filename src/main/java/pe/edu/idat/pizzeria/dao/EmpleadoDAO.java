package pe.edu.idat.pizzeria.dao;

import java.util.List;


import pe.edu.idat.pizzeria.bean.EmpleadoBean;

public interface EmpleadoDAO {
	
	 public EmpleadoBean selEmpleadoByCodigo(String empCodigo) throws Exception;
     
	    public List<EmpleadoBean> SelEmpleado() throws Exception;
	    
	    public boolean insEmpleado(EmpleadoBean empleado)throws Exception;
	    
	    public boolean updEmpleado(EmpleadoBean empleado)throws Exception;
	    
	    public boolean delEmpleado(String empCodigo)throws Exception; 
	    
	    /*public ClienteBean selClienteByCodigoAndPassword(String cliCodigo,String cliPassword) throws Exception;*/
	}
