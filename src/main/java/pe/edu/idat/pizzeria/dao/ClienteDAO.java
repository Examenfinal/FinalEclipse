package pe.edu.idat.pizzeria.dao;

import java.util.List;
import pe.edu.idat.pizzeria.bean.ClienteBean;


public interface ClienteDAO {
	
	 public ClienteBean selClienteByCodigo(String cliCodigo) throws Exception;
     
	    public List<ClienteBean> SelCliente() throws Exception;
	    
	    public boolean insCliente(ClienteBean cliente)throws Exception;
	    
	    public boolean updCliente(ClienteBean cliente)throws Exception;
	    
	    public boolean delCliente(String cliCodigo)throws Exception; 
	    
	    /*public ClienteBean selClienteByCodigoAndPassword(String cliCodigo,String cliPassword) throws Exception;*/
	}



