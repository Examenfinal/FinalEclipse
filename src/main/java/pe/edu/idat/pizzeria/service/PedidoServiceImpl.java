package pe.edu.idat.pizzeria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.pizzeria.bean.PedidoBean;
import pe.edu.idat.pizzeria.dao.PedidoDAOImpl;



@Service("PedidoService")
public class PedidoServiceImpl  implements PedidoService{

	@Autowired
	private PedidoDAOImpl pedidoDAO;	
	
	@Override
	public List<PedidoBean> listarPedido () throws Exception {
		
		List<PedidoBean> lst =pedidoDAO.SelPedido();
		return lst;
	}

}
