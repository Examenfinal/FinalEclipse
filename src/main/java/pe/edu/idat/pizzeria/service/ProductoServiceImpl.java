package pe.edu.idat.pizzeria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.pizzeria.bean.ProductoBean;
import pe.edu.idat.pizzeria.dao.ProductoDAOImpl;



@Service("ProductoService")
public class ProductoServiceImpl implements ProductoService{

	@Autowired
	private ProductoDAOImpl productoDAO;	
	
	@Override
	public List<ProductoBean> listarProducto() throws Exception {
		
		List<ProductoBean> lst =productoDAO.SelProducto();
		return lst;
	}

}

