package pe.edu.idat.pizzeria.service;

import java.util.List;

import java.util.ArrayList;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.idat.pizzeria.bean.ClienteBean;
import pe.edu.idat.pizzeria.dao.ClienteDAOImpl;


@Service("ClienteService")
public class ClienteServiceImpl  implements ClienteService {

	
	// DECLARACIONES SERVICE
		private static final Log log = LogFactory.getLog(ClienteServiceImpl.class);	
		private boolean result;
		ClienteBean clienteBean;	
		
		
	@Autowired 
	private ClienteDAOImpl clienteDAO;
	
	
	// METODO LISTAR TODOS
	@Override
	public List<ClienteBean> listarCliente() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteServiceImpl.listarCliente");
		
		List<ClienteBean> lst = new ArrayList<ClienteBean>();	
		try {
			// LISTAR USUARIOS
			lst = clienteDAO.SelCliente();

		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.listarCliente");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.listarCliente");
		} 	
		return lst;
	}
	
	// METODO LISTAR X CODIGO
		@Override
		public ClienteBean listarClientexCodigo(String cliCodigo) throws Exception {
			try {
				clienteBean = clienteDAO.selClienteByCodigo(cliCodigo);
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.listarCliente");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.listarCliente");
			} 	
			return clienteBean;
		}
	
		// METODO INSERTAR
		@Override
		public boolean insertarCliente(ClienteBean u) throws Exception {
			try {
				//POLITICA DEL SISTEMA
				//u.setCli_nombre(new String());
				
				result = clienteDAO.insCliente(u);		
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.insertarCliente");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.insertarCliente");
			} 	
			return result;
		}
		
		// METODO ACTUALIZAR
		@Override
		public boolean actualizarCliente(ClienteBean u) throws Exception { 	
			try {
				result = clienteDAO.updCliente(u);		
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.actualizarCliente");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.actualizarCliente");
			} 	
			return result;
		}
		
		// METODO ELIMINAR X CODIGO
		@Override
		public boolean eliminarClientexCodigo(String cliCodigo) throws Exception {
			try {
				result = clienteDAO.delCliente(cliCodigo);		
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.eliminarClientexCodigo");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.eliminarClientexCodigo");
			} 	
			return result;
		}
		
		/*// METODO LOGIN
		@Override
		public ClienteBean listarClientexCodigoAndPassword(String cliNombre, String cliPassword) throws Exception {		
			try {
				clienteBean = clienteDAO.selClienteByCodigoAndPassword(cliNombre, cliPassword);			
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.listarClientexCodigoxPassword");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.listarClientexCodigoxPassword");
			} 	
			return clienteBean;
		}	*/
		
		
}	
	
