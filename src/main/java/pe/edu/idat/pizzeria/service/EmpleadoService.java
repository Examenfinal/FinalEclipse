package pe.edu.idat.pizzeria.service;

import java.util.List;


import pe.edu.idat.pizzeria.bean.EmpleadoBean;

public interface EmpleadoService {

	public List<EmpleadoBean> listarEmpleado() throws Exception;	
	public EmpleadoBean listarEmpleadoxCodigo(String empCodigo) throws Exception;
	public boolean insertarEmpleado(EmpleadoBean u) throws Exception;
	public boolean actualizarEmpleado(EmpleadoBean u) throws Exception;
	public boolean eliminarEmpleadoxCodigo(String empCodigo) throws Exception;
	/*public ClienteBean listarClientexCodigoAndPassword(String cliCodigo,String cliPassword) throws Exception;*/
}