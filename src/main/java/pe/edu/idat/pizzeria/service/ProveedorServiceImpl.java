package pe.edu.idat.pizzeria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.pizzeria.bean.ProveedorBean;
import pe.edu.idat.pizzeria.dao.ProveedorDAOImpl;


@Service("ProveedorService")
public class ProveedorServiceImpl implements ProveedorService{

	@Autowired
	private ProveedorDAOImpl proveedorDAO;	
	
	@Override
	public List<ProveedorBean> listarProveedor() throws Exception {
		
		List<ProveedorBean> lst =proveedorDAO.SelProveedor();
		return lst;
	}

}