package pe.edu.idat.pizzeria.service;

import java.util.List;


import pe.edu.idat.pizzeria.bean.PedidoBean;

public interface PedidoService {

	public List<PedidoBean> listarPedido() throws Exception;	
	public PedidoBean listarPedidoxCodigo(String pedCodigo) throws Exception;
	public boolean insertarPedido(PedidoBean u) throws Exception;
	public boolean actualizarPedido(PedidoBean u) throws Exception;
	public boolean eliminarPedidoxCodigo(String pedCodigo) throws Exception;
	/*public ClienteBean listarClientexCodigoAndPassword(String cliCodigo,String cliPassword) throws Exception;*/
}