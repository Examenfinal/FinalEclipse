package pe.edu.idat.pizzeria.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import pe.edu.idat.pizzeria.bean.EmpleadoBean;

import pe.edu.idat.pizzeria.dao.EmpleadoDAOImpl;



@Service("EmpleadoService")
public class EmpleadoServiceImpl  implements EmpleadoService{

	// DECLARACIONES SERVICE
			private static final Log log = LogFactory.getLog(EmpleadoServiceImpl.class);	
			private boolean result;
			EmpleadoBean empleadoBean;	
			
			
		@Autowired 
		private EmpleadoDAOImpl empleadoDAO;
		
		
		// METODO LISTAR TODOS
		@Override
		public List<EmpleadoBean> listarEmpleado() throws Exception {
			if (log.isDebugEnabled()) log.debug("Inicio - EmpleadoServiceImpl.listarEmpleado");
			
			List<EmpleadoBean> lst = new ArrayList<EmpleadoBean>();	
			try {
				// LISTAR USUARIOS
				lst = empleadoDAO.SelEmpleado();

			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - EmpleadoServiceImpl.listarEmpleado");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - EmpleadoServiceImpl.listarEmpleado");
			} 	
			return lst;
		}
		
		// METODO LISTAR X CODIGO
			@Override
			public EmpleadoBean listarEmpleadoxCodigo(String empCodigo) throws Exception {
				try {
					empleadoBean = empleadoDAO.selEmpleadoByCodigo(empCodigo);
				}catch(Exception ex) {
					if (log.isDebugEnabled()) log.debug("Error - EmpleadoServiceImpl.listarEmpleado");
					log.error(ex, ex);
					throw ex;
				}finally{
					if (log.isDebugEnabled()) log.debug("Final - EmpleadoServiceImpl.listarEmpleado");
				} 	
				return empleadoBean;
			}
		
			// METODO INSERTAR
			@Override
			public boolean insertarEmpleado(EmpleadoBean u) throws Exception {
				try {
					//POLITICA DEL SISTEMA
					//u.setCli_nombre(new String());
					
					result = empleadoDAO.insEmpleado(u);		
				}catch(Exception ex) {
					if (log.isDebugEnabled()) log.debug("Error - EmpleadoServiceImpl.insertarEmpleado");
					log.error(ex, ex);
					throw ex;
				}finally{
					if (log.isDebugEnabled()) log.debug("Final - EmpleadoServiceImpl.insertarEmpleado");
				} 	
				return result;
			}
			
			// METODO ACTUALIZAR
			@Override
			public boolean actualizarEmpleado(EmpleadoBean u) throws Exception { 	
				try {
					result = empleadoDAO.updEmpleado(u);		
				}catch(Exception ex) {
					if (log.isDebugEnabled()) log.debug("Error - EmpleadoServiceImpl.actualizarEmpleado");
					log.error(ex, ex);
					throw ex;
				}finally{
					if (log.isDebugEnabled()) log.debug("Final -EmpleadoServiceImpl.actualizarEmpleado");
				} 	
				return result;
			}
			
			// METODO ELIMINAR X CODIGO
			@Override
			public boolean eliminarEmpleadoxCodigo(String empCodigo) throws Exception {
				try {
					result = empleadoDAO.delEmpleado(empCodigo);		
				}catch(Exception ex) {
					if (log.isDebugEnabled()) log.debug("Error - EmpleadoServiceImpl.eliminarEmpleadoxCodigo");
					log.error(ex, ex);
					throw ex;
				}finally{
					if (log.isDebugEnabled()) log.debug("Final - EmpleadoServiceImpl.eliminarEmpleadoxCodigo");
				} 	
				return result;
			}
			
			/*// METODO LOGIN
			@Override
			public ClienteBean listarClientexCodigoAndPassword(String cliNombre, String cliPassword) throws Exception {		
				try {
					clienteBean = clienteDAO.selClienteByCodigoAndPassword(cliNombre, cliPassword);			
				}catch(Exception ex) {
					if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.listarClientexCodigoxPassword");
					log.error(ex, ex);
					throw ex;
				}finally{
					if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.listarClientexCodigoxPassword");
				} 	
				return clienteBean;
			}	*/
			
			
	}	
		
