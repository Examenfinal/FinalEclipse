package pe.edu.idat.pizzeria.service;

import java.util.List;

import pe.edu.idat.pizzeria.bean.ClienteBean;


public interface ClienteService {
	
	public List<ClienteBean> listarCliente() throws Exception;	
	public ClienteBean listarClientexCodigo(String cliCodigo) throws Exception;
	public boolean insertarCliente(ClienteBean u) throws Exception;
	public boolean actualizarCliente(ClienteBean u) throws Exception;
	public boolean eliminarClientexCodigo(String cliCodigo) throws Exception;
	/*public ClienteBean listarClientexCodigoAndPassword(String cliCodigo,String cliPassword) throws Exception;*/
}