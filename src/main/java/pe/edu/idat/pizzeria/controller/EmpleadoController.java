package pe.edu.idat.pizzeria.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.sojo.interchange.json.JsonSerializer;


import javax.servlet.http.HttpSession;


import pe.edu.idat.pizzeria.bean.EmpleadoBean;
import pe.edu.idat.pizzeria.service.EmpleadoService;
import pe.edu.idat.pizzeria.util.AvisosConstantes;
import pe.edu.idat.pizzeria.util.Constantes;


@Controller
@RequestMapping({"Empleado", "Emp" })
public class EmpleadoController {

	// DECLARACIONES CONTROLLER
			@Autowired		
			EmpleadoService empleadoService;

			boolean result;
			
			// METODO GET MOSTRAR WEB -> MODALANDVIEW
			@RequestMapping(value="/Web", method = RequestMethod.GET)
			public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
				ModelAndView modelo = new ModelAndView();
				
				
				try {
					// MODALANDVIEW JSP
					modelo.setViewName("viewEmpleado");
					
					// LISTAR USUARIOS 		
					List<EmpleadoBean> lstEmpleado =empleadoService.listarEmpleado();
					
					// AGREGAR LISTAS AL MODALANDVIEW
					modelo.addObject("listadoEmpleado", lstEmpleado);
					
					
					//TODO AGREGAR LISTAS AL MODALANDVIEW
					modelo.addObject("listadoEmpleado", lstEmpleado);
					
				} catch (Exception e) {
					
					// CONTROL ERROR 500
					modelo.setViewName("view500");
					modelo.addObject(Constantes.MESSAGE_TEXT,e.getLocalizedMessage());
				}
				return modelo;
			}
			
			
			
			// METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
			@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public  @ResponseBody Map<String, Object> getEmpleadoAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				try {
									
					// LISTAR USUARIOS 		
					List<EmpleadoBean> lstEmpleado =empleadoService.listarEmpleado();
					
					mapaResult.put(Constantes.STATUS_TEXT, true);
					mapaResult.put(Constantes.MESSAGE_TEXT,  AvisosConstantes.AVISO_CRUD_READ.replace("{0}", String.valueOf(lstEmpleado.size())));
					mapaResult.put("data", lstEmpleado);
					
				} catch (Exception ex) {			
					ex.printStackTrace();			
					mapaResult.put(Constantes.STATUS_TEXT, false);
					mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
				}	finally {
					
				}	
				return mapaResult;
			}
			
			
			
			// METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
			@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> getEmpleado(@PathVariable String codigo) throws Exception  {	
				Map<String, Object> mapaResult = new HashMap<>();	
				try {
									
					// LISTAR X CODIGO		
					EmpleadoBean empleado =empleadoService.listarEmpleadoxCodigo(codigo);
					mapaResult.put(Constantes.STATUS_TEXT, (empleado != null? true : false));
					mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_READ.replace("{0}",(empleado != null? "1":"0")));
									
					if(empleado != null) {
						mapaResult.put("data", empleado);
					}
				} catch (Exception ex) {			
					ex.printStackTrace();			
					mapaResult.put(Constantes.STATUS_TEXT, false);
					mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
				}	finally {
					
				}		
				return mapaResult;
			}
			
			// METODO POST INSERTAR UN REGISTRO  <- JSON
			
			@RequestMapping(value="/Rest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> postEmpleado(HttpServletRequest request, HttpServletResponse response) throws Exception {
				Map<String, Object> mapaResult = new HashMap<String, Object>();		
				try {
							
					//LECTURA DEL REQUEST BODY JSON
					BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
					StringBuilder jsonEnviado = new StringBuilder();
					String line;
			        while ((line = reader.readLine()) != null) {
			        	jsonEnviado.append(line).append('\n');
			        }
			        //----------------------------------------------
			        // SERIALIZAMOS EL JSON -> CLASS 
					EmpleadoBean empleadoNuevo = (EmpleadoBean) new JsonSerializer().deserialize(jsonEnviado.toString(), EmpleadoBean.class);			
					//----------------------------------------------
					// SERVICE INSERTAR USUARIO
					result =empleadoService.insertarEmpleado(empleadoNuevo);
					//----------------------------------------------
					mapaResult.put(Constantes.STATUS_TEXT, result);		
					mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_INSERT.replace("{0}", (result ? "1": "0")));
				} catch (Exception ex) {
					ex.printStackTrace();
					mapaResult.put(Constantes.STATUS_TEXT, false);
					mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
				}finally {
					
				}				
				return mapaResult;
			}
			
			
			// METODO PUT ACTUALIZAR UN REGISTRO  <- JSON
					@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
				public @ResponseBody Map<String, Object> putEmpleado(HttpServletRequest request, HttpServletResponse response) throws Exception  {
					Map<String, Object> mapaResult = new HashMap<String, Object>();		
					try {		
						
						//LECTURA DEL REQUEST BODY
						BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
						StringBuilder jsonEnviado = new StringBuilder();
						String line;
				        while ((line = reader.readLine()) != null) {
				        	jsonEnviado.append(line).append('\n');
				        }
				        
						EmpleadoBean empleadoNuevo = (EmpleadoBean) new JsonSerializer().deserialize(jsonEnviado.toString(), EmpleadoBean.class);			
						
						// ACTUALIZAR USUARIO
						result = empleadoService.actualizarEmpleado(empleadoNuevo);
						mapaResult.put(Constantes.STATUS_TEXT, result);
						mapaResult.put(Constantes.MESSAGE_TEXT, AvisosConstantes.AVISO_CRUD_UPDATE.replace("{0}", (result == true ? "1": "0")));			
						
					} catch (Exception ex) {
						ex.printStackTrace();
						mapaResult.put(Constantes.STATUS_TEXT, false);
						mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
					}	finally {			
					}			
					return mapaResult;
				}
			
			
				
			
			// METODO DELETE ELIMINAR UN REGISTRO  <- PARAMETER
			@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> delEmpleado(@PathVariable String codigo) throws Exception {
				Map<String, Object> mapaResult = new HashMap<>();		
				try {	
					
					
					// ELIMINAR USUARIO
					result = empleadoService.eliminarEmpleadoxCodigo(codigo);			
					mapaResult.put("status", result);
					mapaResult.put("message", AvisosConstantes.AVISO_CRUD_DELETE.replace("{0}", (result ? "1": "0")));
							
				} catch (Exception ex) {			
					ex.printStackTrace();			
					mapaResult.put(Constantes.STATUS_TEXT, false);
					mapaResult.put(Constantes.MESSAGE_TEXT, ex.getLocalizedMessage());
				}	finally {
					
				}		
				return mapaResult;
			}
			
			
			
			/*
			
			// METODO EXPORTAR XLS
			@RequestMapping(value="/Xls", method = RequestMethod.GET)	
			public ModelAndView expClientesXLS(HttpServletRequest request, HttpServletResponse response) {
				ModelAndView model =  null;
				FileInputStream inputStream =  null;
				FileOutputStream fileOut = null;
				
				String[] sColumns = {"Codigo", "Nombre", "Apellido", "Telefono", "Direccion","Estado"};
				int iFila = 6;
				int iColumna = 1;
				response.setContentType("application/vnd.ms-excel");		

				//CREAR LIBRO XLS
				//try(Workbook workbook = new HSSFWorkbook();) {			
					// LISTAR USUARIOS 		
					List<ClienteBean> lstCliente =clienteService.listarCliente();
					
					//CREAR HOJA XLS
					//Sheet sheet = workbook.createSheet("Cliente");
					
					//CREAR UNA FUENTE DE COLOR AZUL BOLD
					//Font headerFont = workbook.createFont();
					//headerFont.setBold(true);
					//headerFont.setColor(IndexedColors.BLACK.getIndex());
					
					// CREAR UN ESTILO TITULO			
					//CellStyle titleCellStyle = workbook.createCellStyle();
					//titleCellStyle.setFont(headerFont);
					
					//titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
					//titleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
					
					
					// CREAR UN ESTILO CABECERA
					CellStyle headerCellStyle = workbook.createCellStyle();
					
					headerCellStyle.setFont(headerFont);
								
					headerCellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
					headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
					    
					headerCellStyle.setBorderTop(BorderStyle.HAIR);			
					headerCellStyle.setBorderBottom(BorderStyle.THICK);
					headerCellStyle.setBorderLeft(BorderStyle.HAIR);
					headerCellStyle.setBorderRight(BorderStyle.HAIR);
					
					headerCellStyle.setBottomBorderColor(IndexedColors.CORAL.getIndex());
					headerCellStyle.setLeftBorderColor(IndexedColors.CORAL.getIndex());
					headerCellStyle.setRightBorderColor(IndexedColors.CORAL.getIndex());
					headerCellStyle.setTopBorderColor(IndexedColors.CORAL.getIndex());
					
					headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
					headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
					
					
					// CREAR FILA TITULO			
					Row titleRow = sheet.createRow((short) 1);
					Cell cellTitle = titleRow.createCell((short) 1);
					cellTitle.setCellValue("LISTADO DE CLIENTE");
					cellTitle.setCellStyle(titleCellStyle);
					
					// COMBINAR FILAS + COLUMNAS
			        sheet.addMergedRegion(new CellRangeAddress(1,1,1,4));
			        
					// CREAR FILA CABECERA
					Row headerRow = sheet.createRow(iFila - 1);
			
					// CREAR CELDAS PARA A FILA
					for (int col = 0; col < sColumns.length; col++) {		
						Cell cell = headerRow.createCell(col + iColumna);
						cell.setCellValue(sColumns[col]);
						cell.setCellStyle(headerCellStyle);
					}		
					
					
					
					// CREAR LAS FILAS DETALLE
					for (ClienteBean u : lstCliente) {
						Row row = sheet.createRow(iFila++);
			 
						row.createCell(0 + iColumna).setCellValue(u.getCli_codigo());
						row.createCell(1 + iColumna).setCellValue(u.getCli_nombre());
						row.createCell(2 + iColumna).setCellValue(u.getCli_apellido());
						row.createCell(3 + iColumna).setCellValue(u.getCli_telefono());
						row.createCell(4 + iColumna).setCellValue(u.getCli_direccion());
			 
						Cell numberCell = row.createCell(5 + iColumna);
						numberCell.setCellValue(u.getCli_estado());		
					}
					
					
					
			 		// AUTOAJUSTE DE COLUMNS
					sheet.autoSizeColumn(0);
					sheet.autoSizeColumn(1);
					sheet.autoSizeColumn(2);
					sheet.autoSizeColumn(3);
					sheet.autoSizeColumn(4);
					sheet.autoSizeColumn(5);
					
				
				
					}
					
					fileOut.close();
					outStream.close();
					inputStream.close();
								
				} catch (Exception e) {
					e.printStackTrace();
				}
				return model;
			}
			
			
			
			*/
			
			
			
			
		}