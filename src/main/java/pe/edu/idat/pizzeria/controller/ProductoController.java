package pe.edu.idat.pizzeria.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.idat.pizzeria.bean.ProductoBean;
import pe.edu.idat.pizzeria.service.ProductoService;
import pe.edu.idat.pizzeria.util.Constantes;


@Controller
@RequestMapping({"Producto", "Prod" })
public class ProductoController {

	// DECLARACIONES CONTROLLER
			@Autowired		
			ProductoService productoService;

			boolean result;
			
			// METODO GET MOSTRAR WEB -> MODALANDVIEW
			@RequestMapping(value="/Web", method = RequestMethod.GET)
			public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
				ModelAndView modelo = new ModelAndView();
				try {
					// MODALANDVIEW JSP
					modelo.setViewName("viewProducto");
					
					// LISTAR USUARIOS 		
					List<ProductoBean> lstProducto =productoService.listarProducto();
					
					// AGREGAR LISTAS AL MODALANDVIEW
					modelo.addObject("listadoProducto", lstProducto);
					
				} catch (Exception e) {
					// CONTROL ERROR 500
					modelo.setViewName("view500");
					modelo.addObject(Constantes.MESSAGE_TEXT,e.getLocalizedMessage());
				}
				return modelo;
			}
				
		}