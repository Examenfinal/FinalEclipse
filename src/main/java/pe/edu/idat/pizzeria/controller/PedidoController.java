package pe.edu.idat.pizzeria.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.idat.pizzeria.bean.PedidoBean;
import pe.edu.idat.pizzeria.service.PedidoService;
import pe.edu.idat.pizzeria.util.Constantes;


@Controller
@RequestMapping({"Pedido", "Ped" })
public class PedidoController {

	// DECLARACIONES CONTROLLER
			@Autowired	
			PedidoService pedidoService;

			boolean result;
			
			// METODO GET MOSTRAR WEB -> MODALANDVIEW
			@RequestMapping(value="/Web", method = RequestMethod.GET)
			public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
				ModelAndView modelo = new ModelAndView();
				try {
					// MODALANDVIEW JSP
					modelo.setViewName("viewPedido");
					
					// LISTAR USUARIOS 		
					List<PedidoBean> lstPedido =pedidoService.listarPedido();
					
					// AGREGAR LISTAS AL MODALANDVIEW
					modelo.addObject("listadoPedido", lstPedido);
					
				} catch (Exception e) {
					// CONTROL ERROR 500
					modelo.setViewName("view500");
					modelo.addObject(Constantes.MESSAGE_TEXT,e.getLocalizedMessage());
				}
				return modelo;
			}
				
		}